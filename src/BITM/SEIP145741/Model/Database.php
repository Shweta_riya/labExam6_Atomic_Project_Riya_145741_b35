<?php
namespace App\Model;

use PDO;
use PDOException;


class Database
{
    public $conn;
    public $username = "root";
    public $password = "";

    public function __construct()
    {
        try {

            $this->conn = new PDO("mysql:host=localhost;dbname=atomic_project_35", $this->username, $this->password);
          //  echo " Connected Successfully";
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}